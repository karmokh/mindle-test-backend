import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Person, PersonDocument } from './modules/person/entities/person.entity';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { Product, ProductDocument } from './modules/product/entities/product.entity';
import { personFaker } from './faker/person.faker';
import { productFaker } from './faker/product.faker';

@Injectable()
export class AppService {
  constructor(
    @InjectModel(Person.name)
    private personModel: SoftDeleteModel<PersonDocument>,
    @InjectModel(Product.name)
    private productModel: SoftDeleteModel<ProductDocument>,
  ) {
  }

  checkHealth(): string {
    return 'Server is running...';
  }

  async faker() {
    await this.personModel.create(personFaker);
    await this.productModel.create(productFaker);

    return 'Fake data inserted';
  }
}
