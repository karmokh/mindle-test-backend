import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {ConfigModule} from '@nestjs/config';
import {MongooseModule} from "@nestjs/mongoose";
import {TranslatorModule} from "nestjs-translator";
import {PersonModule} from "./modules/person/person.module";
import {ProductModule} from "./modules/product/product.module";
import {InvoiceModule} from "./modules/invoice/invoice.module";
import { PersonSchema } from './modules/person/entities/person.entity';
import { ProductSchema } from './modules/product/entities/product.entity';

@Module({
    imports: [
        ConfigModule.forRoot(),
        MongooseModule.forRoot(process.env.MONGODB_URI, {
            dbName: process.env.MONGODB_NAME,
            directConnection: true,
        }),
        TranslatorModule.forRoot({
            global: true,
            defaultLang: 'en',
            translationSource: './src/i18n'
        }),
        MongooseModule.forFeature([{ name: 'Person', schema: PersonSchema }]),
        MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema }]),
        PersonModule,
        ProductModule,
        InvoiceModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
