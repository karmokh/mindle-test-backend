import { IsMongoId, IsNotEmpty } from 'class-validator';

export class IdParameterDto {
  @IsNotEmpty()
  @IsMongoId()
  id: string;
}
