import { faker } from '@faker-js/faker';
import { CreateProductDto } from '../modules/product/dto/create-product.dto';

export function createRandomProduct(): CreateProductDto {
  return {
    name: faker.commerce.productName(),
    price: Number(faker.commerce.price()),
  };
}

export const productFaker = faker.helpers.multiple(createRandomProduct, { count: 20 });