import { faker } from '@faker-js/faker';
import { CreatePersonDto } from '../modules/person/dto/create-person.dto';

export function createRandomPerson(): CreatePersonDto {
  return {
    firstName: faker.person.firstName(),
    lastName: faker.person.lastName(),
    addresses: [
      { id: faker.database.mongodbObjectId(), title: faker.location.city(), address: faker.address.streetAddress() },
      { id: faker.database.mongodbObjectId(), title: faker.location.city(), address: faker.address.streetAddress() },
      { id: faker.database.mongodbObjectId(), title: faker.location.city(), address: faker.address.streetAddress() },
    ],
  };
}

export const personFaker = faker.helpers.multiple(createRandomPerson, { count: 20 });