import {TranslatorService} from 'nestjs-translator';
import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {CreateProductDto} from './dto/create-product.dto';
import {UpdateProductDto} from './dto/update-product.dto';
import {InjectModel} from "@nestjs/mongoose";
import {SoftDeleteModel} from "soft-delete-plugin-mongoose";
import {Product, ProductDocument} from "./entities/product.entity";

@Injectable()
export class ProductService {
    constructor(
        @InjectModel(Product.name)
        private productModel: SoftDeleteModel<ProductDocument>,
        private readonly translator: TranslatorService,
    ) {
    }

    private async exists(query) {
        const product = await this.productModel.exists(query);

        if (!product) {
            throw new HttpException(
                this.translator.translate('id_not_exist'),
                HttpStatus.NOT_FOUND
            );
        }

        return product;
    }

    async create(createProductDto: CreateProductDto) {
        const product = await new this.productModel({...createProductDto}).save();
        return product;
    }

    async findAll(page: number, limit: number) {
        let filter = {}

        let total = null;
        let lastPage = null;
        let data = null;

        if (page === null && limit === null) {
            data = await this.productModel.find(filter).lean();
        } else {
            data = await this.productModel
                .find(filter)
                .skip((page - 1) * limit)
                .limit(limit)
                .lean();
            total = await this.productModel.count(filter);
            lastPage = Math.ceil(total / limit);
        }

        return {
            data,
            total: total,
            page: page,
            limit: limit,
            last_page: lastPage,
        };
    }

    async findOne(id: string) {
        await this.exists({_id: id});

        let product = await this.productModel.findOne({_id: id});
        return product
    }

    async update(id: string, updateProductDto: UpdateProductDto) {
        await this.exists({_id: id});

        const product = await this.productModel.updateOne({_id: id}, {...updateProductDto}, {new: true});
        return product;
    }

    async remove(id: string) {
        await this.exists({_id: id});

        const deleted = await this.productModel.softDelete({_id: id});
        return deleted;
    }

    async removeMultiple(id: string[]) {
        await this.exists({_id: {$in: id}});

        const deleted = await this.productModel.softDelete({_id: {$in: id}});
        return deleted;
    }
}
