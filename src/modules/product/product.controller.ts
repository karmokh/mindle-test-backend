import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Request } from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { IdParameterDto } from '../../common/dto/id-parameter.dto';

@Controller('products')
export class ProductController {
  constructor(private readonly productService: ProductService) {
  }

  @Post()
  create(@Body() createProductDto: CreateProductDto) {
    return this.productService.create(createProductDto);
  }

  @Get('')
  findAll(@Query('page') page: number, @Query('limit') limit: number) {
    return this.productService.findAll(page, limit);
  }

  @Get(':id')
  findOne(@Param() { id }: IdParameterDto) {
    return this.productService.findOne(id);
  }

  @Patch(':id')
  update(@Param() { id }: IdParameterDto, @Body() updateProductDto: UpdateProductDto) {
    return this.productService.update(id, updateProductDto);
  }

  @Delete('multiple')
  removeMultiple(@Request() req) {
    return this.productService.removeMultiple(req.body);
  }

  @Delete(':id')
  remove(@Param() { id }: IdParameterDto) {
    return this.productService.remove(id);
  }
}
