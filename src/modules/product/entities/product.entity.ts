import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document} from "mongoose";
import {softDeletePlugin} from "soft-delete-plugin-mongoose";

export type ProductDocument = Product & Document;

@Schema({timestamps: true})
export class Product {
    @Prop()
    name: string;

    @Prop()
    price: number;
}

export const ProductSchema = SchemaFactory.createForClass(Product).plugin(softDeletePlugin);