import {IsNotEmpty, IsNumber, IsString, Length} from "class-validator";

export class CreateProductDto {
    @IsNotEmpty()
    @IsString()
    @Length(2, 64)
    name: string;

    @IsNotEmpty()
    @IsNumber()
    price: number;
}
