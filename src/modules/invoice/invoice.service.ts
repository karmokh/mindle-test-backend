import { TranslatorService } from 'nestjs-translator';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateInvoiceDto } from './dto/create-invoice.dto';
import { UpdateInvoiceDto } from './dto/update-invoice.dto';
import { InjectModel } from '@nestjs/mongoose';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { Invoice, InvoiceDocument } from './entities/invoice.entity';

@Injectable()
export class InvoiceService {
  constructor(
    @InjectModel(Invoice.name)
    private invoiceModel: SoftDeleteModel<InvoiceDocument>,
    private readonly translator: TranslatorService,
  ) {
  }

  private async exists(query) {
    const invoice = await this.invoiceModel.exists(query);

    if (!invoice) {
      throw new HttpException(
        this.translator.translate('id_not_exist'),
        HttpStatus.NOT_FOUND,
      );
    }

    return invoice;
  }

  async create(createInvoiceDto: CreateInvoiceDto) {
    const invoice = await new this.invoiceModel({ ...createInvoiceDto }).save();
    return invoice;
  }

  async findAll(page: number, limit: number) {
    let filter = {};

    let total = null;
    let lastPage = null;
    let data = null;

    if (page === null && limit === null) {
      data = await this.invoiceModel.find(filter)
        .populate('personId', '_id firstName lastName')
        .populate({
          path: 'items',
          populate: {
            path: 'productId',
            select: '_id name price',
            model: 'Product',
          },
        })
        .lean();
    } else {
      data = await this.invoiceModel
        .find(filter)
        .populate('personId', '_id firstName lastName')
        .populate({
          path: 'items',
          populate: {
            path: 'productId',
            select: '_id name price',
            model: 'Product',
          },
        })
        .skip((page - 1) * limit)
        .limit(limit)
        .lean();
      total = await this.invoiceModel.count(filter);
      lastPage = Math.ceil(total / limit);
    }

    return {
      data,
      total: total,
      page: page,
      limit: limit,
      last_page: lastPage,
    };
  }

  async findOne(id: string) {
    await this.exists({ _id: id });

    let invoice = await this.invoiceModel.findOne({ _id: id });
    return invoice;
  }

  async update(id: string, updateInvoiceDto: UpdateInvoiceDto) {
    await this.exists({ _id: id });

    const invoice = await this.invoiceModel.updateOne({ _id: id }, { ...updateInvoiceDto }, { new: true });
    return invoice;
  }

  async remove(id: string) {
    await this.exists({ _id: id });

    const deleted = await this.invoiceModel.softDelete({ _id: id });
    return deleted;
  }

  async removeMultiple(id: string[]) {
    await this.exists({ _id: { $in: id } });

    const deleted = await this.invoiceModel.softDelete({ _id: { $in: id } });
    return deleted;
  }
}
