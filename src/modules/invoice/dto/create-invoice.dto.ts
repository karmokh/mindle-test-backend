import { ArrayMinSize, IsArray, IsMongoId, IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateInvoiceDto {
  @IsNotEmpty()
  @IsMongoId()
  personId: string;

  @IsNotEmpty()
  address: string;

  @IsArray()
  @ArrayMinSize(1)
  @ValidateNested()
  @Type(() => InvoiceItemDto)
  items: InvoiceItemDto[];
}

class InvoiceItemDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  @IsMongoId()
  productId: string;

  @IsNotEmpty()
  quantity: number;

  @IsNotEmpty()
  price: number;
};
