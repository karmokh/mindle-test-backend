import {Prop, Schema} from '@nestjs/mongoose';
import mongoose from "mongoose";
import {Product} from "../../product/entities/product.entity";

@Schema({timestamps: false})
export class InvoiceItem {
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: Product.name})
    productId: string;

    @Prop()
    quantity: number;

    @Prop()
    price: number;
}