import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import mongoose, {Document} from "mongoose";
import {softDeletePlugin} from "soft-delete-plugin-mongoose";
import {Person} from "../../person/entities/person.entity";
import {InvoiceItem} from "./invoice-item.schema";

export type InvoiceDocument = Invoice & Document;

@Schema({timestamps: true})
export class Invoice {
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: Person.name})
    personId: string;

    @Prop()
    address: string;

    @Prop()
    items: InvoiceItem[];
}

export const InvoiceSchema = SchemaFactory.createForClass(Invoice).plugin(softDeletePlugin);