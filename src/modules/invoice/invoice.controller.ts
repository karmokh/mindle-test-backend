import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Request } from '@nestjs/common';
import { InvoiceService } from './invoice.service';
import { CreateInvoiceDto } from './dto/create-invoice.dto';
import { UpdateInvoiceDto } from './dto/update-invoice.dto';
import { IdParameterDto } from '../../common/dto/id-parameter.dto';

@Controller('invoices')
export class InvoiceController {
  constructor(private readonly invoiceService: InvoiceService) {
  }

  @Post()
  create(@Body() createInvoiceDto: CreateInvoiceDto) {
    return this.invoiceService.create(createInvoiceDto);
  }

  @Get('')
  findAll(@Query('page') page: number, @Query('limit') limit: number) {
    return this.invoiceService.findAll(page, limit);
  }

  @Get(':id')
  findOne(@Param() { id }: IdParameterDto) {
    return this.invoiceService.findOne(id);
  }

  @Patch(':id')
  update(@Param() { id }: IdParameterDto, @Body() updateInvoiceDto: UpdateInvoiceDto) {
    return this.invoiceService.update(id, updateInvoiceDto);
  }

  @Delete('multiple')
  removeMultiple(@Request() req) {
    return this.invoiceService.removeMultiple(req.body);
  }

  @Delete(':id')
  remove(@Param() { id }: IdParameterDto) {
    return this.invoiceService.remove(id);
  }
}
