import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ timestamps: false })
export class PersonAddress {
  @Prop()
  title: string;

  @Prop()
  address: string;
}