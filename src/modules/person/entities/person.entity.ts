import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document} from "mongoose";
import {softDeletePlugin} from "soft-delete-plugin-mongoose";
import {PersonAddress} from "./person-address.schema";

export type PersonDocument = Person & Document;

@Schema({timestamps: true})
export class Person {
    @Prop()
    firstName: string;

    @Prop()
    lastName: string;

    @Prop()
    addresses: PersonAddress[];
}

export const PersonSchema = SchemaFactory.createForClass(Person).plugin(softDeletePlugin);