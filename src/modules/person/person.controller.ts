import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Request } from '@nestjs/common';
import { PersonService } from './person.service';
import { CreatePersonDto } from './dto/create-person.dto';
import { UpdatePersonDto } from './dto/update-person.dto';
import { IdParameterDto } from '../../common/dto/id-parameter.dto';

@Controller('persons')
export class PersonController {
  constructor(private readonly personService: PersonService) {
  }

  @Post()
  create(@Body() createPersonDto: CreatePersonDto) {
    return this.personService.create(createPersonDto);
  }

  @Get('')
  findAll(@Query('page') page: number, @Query('limit') limit: number) {
    return this.personService.findAll(page, limit);
  }

  @Get(':id')
  findOne(@Param() { id }: IdParameterDto) {
    return this.personService.findOne(id);
  }

  @Patch(':id')
  update(@Param() { id }: IdParameterDto, @Body() updatePersonDto: UpdatePersonDto) {
    return this.personService.update(id, updatePersonDto);
  }

  @Delete('multiple')
  removeMultiple(@Request() req) {
    return this.personService.removeMultiple(req.body);
  }

  @Delete(':id')
  remove(@Param() { id }: IdParameterDto) {
    return this.personService.remove(id);
  }
}
