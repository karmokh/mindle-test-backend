import { IsArray, IsNotEmpty, IsOptional, IsString, Length, ValidateNested } from 'class-validator';
import { PersonAddress } from '../entities/person-address.schema';
import { Type } from 'class-transformer';

export class CreatePersonDto {
  @IsNotEmpty()
  @IsString()
  @Length(2, 32)
  firstName: string;

  @IsNotEmpty()
  @IsString()
  @Length(2, 32)
  lastName: string;

  @IsOptional()
  @IsArray()
  @ValidateNested()
  @Type(() => PersonAddressDto)
  addresses: PersonAddressDto[];
}

class PersonAddressDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  address: string;
}
