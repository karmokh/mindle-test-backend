import {TranslatorService} from 'nestjs-translator';
import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {CreatePersonDto} from './dto/create-person.dto';
import {UpdatePersonDto} from './dto/update-person.dto';
import {InjectModel} from "@nestjs/mongoose";
import {SoftDeleteModel} from "soft-delete-plugin-mongoose";
import {Person, PersonDocument} from "./entities/person.entity";

@Injectable()
export class PersonService {
    constructor(
        @InjectModel(Person.name)
        private personModel: SoftDeleteModel<PersonDocument>,
        private readonly translator: TranslatorService,
    ) {
    }

    private async exists(query) {
        const person = await this.personModel.exists(query);

        if (!person) {
            throw new HttpException(
                this.translator.translate('id_not_exist'),
                HttpStatus.NOT_FOUND
            );
        }

        return person;
    }

    async create(createPersonDto: CreatePersonDto) {
        const person = await new this.personModel({...createPersonDto}).save();
        return person;
    }

    async findAll(page: number, limit: number) {
        let filter = {}

        let total = null;
        let lastPage = null;
        let data = null;

        if (page === null && limit === null) {
            data = await this.personModel.find(filter).lean();
        } else {
            data = await this.personModel
                .find(filter)
                .skip((page - 1) * limit)
                .limit(limit)
                .lean();
            total = await this.personModel.count(filter);
            lastPage = Math.ceil(total / limit);
        }

        return {
            data,
            total: total,
            page: page,
            limit: limit,
            last_page: lastPage,
        };
    }

    async findOne(id: string) {
        await this.exists({_id: id});

        let person = await this.personModel.findOne({_id: id});
        return person
    }

    async update(id: string, updatePersonDto: UpdatePersonDto) {
        await this.exists({_id: id});

        const person = await this.personModel.updateOne({_id: id}, {...updatePersonDto}, {new: true});
        return person;
    }

    async remove(id: string) {
        await this.exists({_id: id});

        const deleted = await this.personModel.softDelete({_id: id});
        return deleted;
    }

    async removeMultiple(id: string[]) {
        await this.exists({_id: {$in: id}});

        const deleted = await this.personModel.softDelete({_id: {$in: id}});
        return deleted;
    }
}
